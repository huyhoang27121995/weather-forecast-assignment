import axios from 'axios';

const API_KEY = 'dfcf98b3727b4ba0991153130232906';

const URL = 'https://api.weatherapi.com/v1/forecast.json';

export const fetchWeather = async (city, dayNumber) => {
  try {
    const response = await axios.get(`${URL}?key=${API_KEY}&q=${city}&days=${dayNumber}`);
    return response.data;
  } catch (error) {
    console.error('Error fetching weather:', error);
    throw error;
  }
};
