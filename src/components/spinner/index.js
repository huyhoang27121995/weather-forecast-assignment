import React from 'react';
import { CircularProgress, Box, styled } from '@mui/material';

const SpinnerBox = styled(Box)({
  position: 'fixed',
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  zIndex: 9999,
  background: 'rgba(0, 0, 0, 0.5)'
});

const SpinnerWrapper = styled(Box)({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '100vh'
});

const WeatherForecastSpinner = () => {
  return (
    <SpinnerBox>
      <SpinnerWrapper>
        <CircularProgress />
      </SpinnerWrapper>
    </SpinnerBox>
  );
};

export default WeatherForecastSpinner;
