import React from 'react';
import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useForm, Controller } from 'react-hook-form';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import { CloseOutlined } from '@ant-design/icons';
import { SingaporeLocation } from 'data/location';

const WeatherDialog = ({ open, title, onSubmit, onClose }) => {
  const {
    handleSubmit,
    register,
    control,
    reset,
    formState: { errors }
  } = useForm({
    defaultValues: {
      days: 1
    }
  });

  const days = Array.from({ length: 7 }, (_, index) => index + 1);

  const handleFormSubmit = (data) => {
    onSubmit(data);
  };

  const validationSchema = {
    city: {
      required: 'City is required'
    }
  };

  useEffect(() => {
    if (!open) reset({ days: 1 });
  }, [open, reset]);

  return (
    <Box>
      <Dialog
        open={open}
        sx={{
          '& .MuiPaper-root': {
            minWidth: '300px',
            maxWidth: '500px'
          }
        }}
      >
        <form onSubmit={handleSubmit(handleFormSubmit)}>
          <DialogTitle
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center'
            }}
          >
            {title}
            <CloseOutlined onClick={onClose} />
          </DialogTitle>
          <DialogContent
            sx={{
              paddingBottom: '10px'
            }}
          >
            <FormControl
              fullWidth
              sx={{
                marginBottom: '10px',
                marginTop: '5px'
              }}
            >
              <Autocomplete
                sx={{
                  '& .MuiInputBase-root': {
                    height: '40px',
                    paddingTop: '5px'
                  },
                  '& label': {
                    height: '20px'
                  }
                }}
                fullWidth
                freeSolo
                options={SingaporeLocation}
                renderInput={(params) => <TextField {...register('city', validationSchema.city)} {...params} label="Search City" />}
              />
              {errors.city && (
                <>
                  <span role="alert" style={{ color: 'red' }}>
                    {errors.city.message}
                  </span>
                  <br />
                </>
              )}
            </FormControl>
            <FormControl fullWidth>
              <InputLabel>Number of days</InputLabel>
              <Controller
                name="days"
                control={control}
                defaultValue={1}
                render={({ field }) => (
                  <Select
                    sx={{
                      '& .MuiInputBase-root': {
                        height: '40px'
                      },
                      '& label': {
                        height: '20px'
                      }
                    }}
                    {...field}
                    label="Select number of days"
                    variant="outlined"
                  >
                    {days.map((day) => (
                      <MenuItem key={day} value={day}>
                        {day} day{day > 1 ? 's' : ''}
                      </MenuItem>
                    ))}
                  </Select>
                )}
              />
              {errors.days && (
                <span role="alert" style={{ color: 'red' }}>
                  {errors.days.message}
                </span>
              )}
            </FormControl>
          </DialogContent>
          <DialogActions sx={{ padding: '0px 25px', marginBottom: '10px' }}>
            <Button type="submit" variant="contained">
              Ok
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </Box>
  );
};

WeatherDialog.propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  onSubmit: PropTypes.func,
  onClose: PropTypes.func
};

export default WeatherDialog;
