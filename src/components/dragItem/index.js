import PropTypes from 'prop-types';
import { Responsive, WidthProvider } from 'react-grid-layout';

const ResponsiveReactGridLayout = WidthProvider(Responsive);

const DragItem = ({ children, widgetList }) => {
  const generateLayout = () => {
    return widgetList.map((_widget, index) => ({
      i: `widget-${index}`,
      x: (index * 4) % 12,
      y: Math.floor(index / 3),
      w: 4,
      h: 1
    }));
  };

  const layout = generateLayout();

  return (
    <ResponsiveReactGridLayout
      className="layout"
      rowHeight={850}
      layouts={{ lg: layout }}
      breakpoints={{ lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0 }}
      cols={{ lg: 12, md: 12, sm: 8, xs: 8, xxs: 4 }}
      isBounded={true}
      measureBeforeMount={true}
      margin={[20, 20]}
    >
      {children}
    </ResponsiveReactGridLayout>
  );
};

DragItem.propTypes = {
  children: PropTypes.node.isRequired,
  widgetList: PropTypes.array.isRequired
};

export default DragItem;
