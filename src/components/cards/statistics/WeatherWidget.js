import PropTypes from 'prop-types';
import { useState } from 'react';
import { FieldTimeOutlined, AntCloudOutlined } from '@ant-design/icons';
import WindPowerIcon from '@mui/icons-material/WindPower';
// material-ui
import {
  Box,
  Chip,
  Grid,
  Stack,
  Typography,
  ListItemButton,
  ListItemSecondaryAction,
  List,
  ListItemAvatar,
  ListItemText,
  Avatar,
  Tabs,
  Tab,
  Select,
  MenuItem,
  InputLabel,
  FormControl
} from '@mui/material';

import { CloseOutlined } from '@ant-design/icons';

// project import
import MainCard from 'components/MainCard';

// assets
import AreaChart from '../../../pages/dashboard/AreaChart';

const WeatherWidget = ({ data, color, onClose }) => {
  const unitOptions = [
    { value: 'temp_c', name: 'Temperature (℃)' },
    { value: 'wind_kph', name: 'Wind (kph)' },
    { value: 'cloud', name: 'Cloud' }
  ];

  const [selectedTab, setSelectedTab] = useState(0);
  const [unitSelected, setUnitSelected] = useState(unitOptions[0].value);

  const handleChangeUnitSelected = (event) => {
    setUnitSelected(event.target.value);
  };

  const handleTabChange = (_event, newValue) => {
    setSelectedTab(newValue);
  };

  const convertToDayOfWeek = (dateStr) => {
    const date = new Date(dateStr);
    const options = { weekday: 'long' };
    return date.toLocaleString('en-US', options).substring(0, 3);
  };

  return (
    <Box
      sx={{
        width: '100%',
        height: '100%'
      }}
    >
      <MainCard contentSX={{ p: 2.25 }}>
        <Stack spacing={0.5}>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: '100%'
            }}
          >
            <Typography variant="h6" color="textSecondary">
              {data?.location?.name}
            </Typography>
            <CloseOutlined
              onClick={onClose}
              style={{
                cursor: 'pointer',
                fontSize: '18px'
              }}
            />
          </Box>
          <Grid container alignItems="center">
            <Grid item>
              <Typography variant="h4" color="inherit">
                {data?.current?.temp_c} ℃
              </Typography>
            </Grid>
            <Grid item>
              <Chip
                variant="combined"
                icon={<FieldTimeOutlined />}
                color={color}
                label={data?.current?.last_updated}
                sx={{ ml: 1.25, pl: 1 }}
                size="small"
              />
            </Grid>
          </Grid>
        </Stack>
        <Box sx={{ pt: 2.25 }}>
          <Typography variant="caption" color="textSecondary">
            Weather Conditions Today
          </Typography>
        </Box>
        <Box>
          <List>
            <ListItemButton>
              <ListItemAvatar>
                <Avatar
                  sx={{
                    color: 'success.main',
                    bgcolor: 'success.lighter'
                  }}
                >
                  <AntCloudOutlined />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={<Typography variant="subtitle1">Cloud</Typography>} secondary="Cloud cover as percentage" />
              <ListItemSecondaryAction>
                <Stack alignItems="flex-end">
                  <Typography variant="subtitle1" noWrap>
                    {data?.current?.cloud}
                  </Typography>
                </Stack>
              </ListItemSecondaryAction>
            </ListItemButton>
            <ListItemButton>
              <ListItemAvatar>
                <Avatar
                  sx={{
                    color: 'primary.main',
                    bgcolor: 'success.lighter'
                  }}
                >
                  <WindPowerIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={<Typography variant="subtitle1">Wind</Typography>}
                secondary="Maximum wind speed in kilometer per hour"
                sx={{
                  '& .MuiListItemText-secondary': {
                    maxWidth: '200px'
                  }
                }}
              />
              <ListItemSecondaryAction>
                <Stack alignItems="flex-end">
                  <Typography variant="subtitle1" noWrap>
                    {data?.current?.wind_kph}
                  </Typography>
                </Stack>
              </ListItemSecondaryAction>
            </ListItemButton>
          </List>
        </Box>
        <Box sx={{ pt: 2.25, pb: 2.5 }}>
          <Typography variant="caption" color="textSecondary">
            Weather forecast predicts hourly conditions for the entire day
          </Typography>
        </Box>
        <Box>
          <FormControl>
            <InputLabel>Weather Unit</InputLabel>
            <Select
              onChange={handleChangeUnitSelected}
              sx={{
                width: '200px'
              }}
              label="Weather Unit"
              value={unitSelected}
            >
              {unitOptions.map((unit) => (
                <MenuItem key={unit.value} value={unit.value}>
                  {unit.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Box>
        <Box>
          <Tabs value={selectedTab} onChange={handleTabChange} centered sx={{ overflowX: 'auto' }}>
            {data.forecast.forecastday.map((forecast, index) => (
              <Tab
                key={index}
                label={convertToDayOfWeek(forecast.date)}
                sx={{
                  minWidth: '50px',
                  padding: '12px 10px'
                }}
              />
            ))}
          </Tabs>
          <div>
            <AreaChart
              data={data.forecast.forecastday[selectedTab].hour}
              unit={unitSelected}
              unitNote={unitOptions.find((unit) => unit.value === unitSelected).name}
            />
          </div>
        </Box>
      </MainCard>
    </Box>
  );
};

WeatherWidget.propTypes = {
  data: PropTypes.object,
  color: PropTypes.string,
  onClose: PropTypes.func
};

WeatherWidget.defaultProps = {
  color: 'primary'
};

export default WeatherWidget;
