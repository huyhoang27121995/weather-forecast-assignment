export const SingaporeLocation = [
  'Kampong Serangoon Kechil',
  'Jalan Kayu',
  'Bukit Sembawang Estate',
  'Choa Chu Kang New Town',
  'Keat Hong Village',
  'Bukit Panjang Estate',
  'Kampong Cutforth',
  'Matilda Estate',
  'Serangoon',
  'Kampong Pinang',
  'Kampong Tongkang Pechah',
  'Kampong Teban',
  'Kampong Sungai Tengah',
  'Kangkar'
];
