import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import ReactApexChart from 'react-apexcharts';

const AreaChart = ({ data, unit, unitNote }) => {
  const [series, setSeries] = useState([{ data: [] }]);
  const [options, setOptions] = useState({
    chart: {
      type: 'area',
      height: 365,
      toolbar: {
        show: false
      }
    },
    plotOptions: {
      area: {
        borderRadius: 4
      }
    },
    dataLabels: {
      enabled: false
    },
    xaxis: {
      categories: [],
      axisBorder: {
        show: false
      },
      axisTicks: {
        show: false
      }
    },
    grid: {
      show: false
    },
    tooltip: {
      enabled: true,
      style: {
        fontSize: '12px',
        fontFamily: 'Roboto, sans-serif'
      },
      x: {
        formatter: () => 'Information'
      }
    }
  });

  const convertToHourTime = (timeString) => {
    const date = new Date(timeString);
    return date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
  };

  useEffect(() => {
    const dates = data.map((data) => convertToHourTime(data.time));
    const avgTemps = data.map((data) => data[unit]);

    setOptions((prevState) => ({
      ...prevState,
      xaxis: {
        ...prevState.xaxis,
        categories: dates
      }
    }));

    setSeries([{ data: avgTemps, name: unitNote }]);
  }, [data, unit, unitNote]);

  return (
    <div>
      <ReactApexChart options={options} series={series} type="area" height={365} />
    </div>
  );
};

AreaChart.propTypes = {
  data: PropTypes.array,
  unitNote: PropTypes.string,
  unit: PropTypes.string
};

export default AreaChart;
