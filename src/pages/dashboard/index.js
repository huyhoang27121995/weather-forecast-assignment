import { useState, useEffect } from 'react';
// material-ui
import { Grid, Button, Typography, FormControl, Select, InputLabel, MenuItem, Box, Autocomplete, TextField, styled } from '@mui/material';

import { PlusOutlined } from '@ant-design/icons';

import WeatherWidget from 'components/cards/statistics/WeatherWidget';

import WeatherDialog from 'components/dialog/weather/WeatherDialog';

import { fetchWeather } from 'services/weather.service';

import WeatherForecastSpinner from 'components/spinner';

import DragItem from 'components/dragItem';

import { SingaporeLocation } from 'data/location';

const FormSelectDay = styled(FormControl)(() => ({
  marginLeft: '30px',
  height: '40x',
  width: '200px',
  '& label': {
    overflow: 'unset'
  },
  '@media (max-width: 576px)': {
    marginTop: '10px !important',
    marginLeft: 0,
    width: '100%'
  },
  '@media (max-width: 768px)': {
    marginTop: 0,
    marginLeft: '5px'
  }
}));

const FormSelectCity = styled(FormControl)(() => ({
  width: '250px',
  marginLeft: '10px',
  '& label': {
    overflow: 'unset'
  },
  '@media (max-width: 576px)': {
    marginTop: '10px !important',
    marginLeft: 0,
    width: '100%'
  },
  '@media (max-width: 768px)': {
    marginTop: 0,
    marginLeft: '5px'
  }
}));

const DashboardDefault = () => {
  const [weatherForecastList, setWeatherForecastList] = useState([]);
  const [openWeatherDialog, setOpenWeatherDialog] = useState(false);
  const [dayNumber, setDayNumber] = useState('');
  const [searchedCity, setSearchedCity] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const dayOptions = Array.from({ length: 7 }, (_, index) => index + 1);

  const handleChangeDayNumber = (event) => {
    setDayNumber(event.target.value);
  };

  const handleSearchCity = (_event, value) => {
    setSearchedCity(value);
  };

  const getFilteredWidgets = () => {
    return searchedCity ? weatherForecastList.filter((item) => item.location.name === searchedCity) : weatherForecastList;
  };

  const updateWidgets = async () => {
    setIsLoading(true);
    const updatedWidgets = await Promise.all(
      weatherForecastList.map(async (widget) => {
        const updatedWidget = await fetchWeather(widget.location.name, dayNumber);
        return updatedWidget;
      })
    );
    setWeatherForecastList(updatedWidgets);
    setIsLoading(false);
  };

  const handleCreateWidget = async (data) => {
    setIsLoading(true);
    const createdWidget = await fetchWeather(data.city, data.days);
    setWeatherForecastList((prevList) => [...prevList, createdWidget]);
    setIsLoading(false);
    setOpenWeatherDialog(false);
  };

  const handleDeleteWidget = (index) => {
    setWeatherForecastList((prevList) => {
      const updatedList = [...prevList];
      updatedList.splice(index, 1);
      return updatedList;
    });
  };

  useEffect(() => {
    updateWidgets();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dayNumber]);

  useEffect(() => {
    const refreshInterval = setInterval(updateWidgets, 60 * 60 * 1000);

    return () => {
      clearInterval(refreshInterval);
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dayNumber]);

  return (
    <Grid container rowSpacing={4.5} columnSpacing={2.75}>
      {isLoading && <WeatherForecastSpinner />}
      <Grid item xs={12} sx={{ mb: -2.25 }}>
        <Button
          sx={{
            borderRadius: '25px',
            backgroundColor: '#25bc25',
            padding: '8px 20px',
            color: '#FFFFFF',
            '&:hover': {
              backgroundColor: '#18c418'
            }
          }}
          onClick={() => setOpenWeatherDialog(true)}
        >
          <PlusOutlined />
          <Typography
            sx={{
              marginLeft: '5px'
            }}
          >
            Add Widget
          </Typography>
        </Button>
        <FormSelectDay>
          <InputLabel
            sx={{
              color: weatherForecastList.length === 0 ? '#bfbfbf' : '#262626'
            }}
          >
            Number of days
          </InputLabel>
          <Select value={dayNumber} label="Number of days" disabled={weatherForecastList.length === 0} onChange={handleChangeDayNumber}>
            {dayOptions.map((day) => (
              <MenuItem key={day} value={day}>
                {day} day{day > 1 ? 's' : ''}
              </MenuItem>
            ))}
          </Select>
        </FormSelectDay>
        <FormSelectCity>
          <Autocomplete
            fullWidth
            freeSolo
            options={SingaporeLocation}
            disabled={weatherForecastList.length === 0}
            onChange={handleSearchCity}
            renderInput={(params) => <TextField {...params} label="Search City" />}
            sx={{
              '& .MuiOutlinedInput-root': {
                height: '40px',
                padding: '5px'
              }
            }}
          />
        </FormSelectCity>
      </Grid>
      <WeatherDialog
        open={openWeatherDialog}
        title="Add Weather Forecast Widget"
        onSubmit={(data) => handleCreateWidget(data)}
        onClose={() => setOpenWeatherDialog(false)}
      />

      {weatherForecastList && (
        <DragItem widgetList={getFilteredWidgets()}>
          {getFilteredWidgets().map((widget, index) => (
            <Box key={`widget-${index}`}>
              <WeatherWidget data={widget} onClose={() => handleDeleteWidget(index)} />
            </Box>
          ))}
        </DragItem>
      )}
    </Grid>
  );
};

export default DashboardDefault;
